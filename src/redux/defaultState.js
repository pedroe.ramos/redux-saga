import { fromJS } from 'immutable'
// export const defaultState = fromJS({
//   user: null,
//   items: {
//     list: [],
//     id: null
//   },
//   users: {
//     list: [],
//     id: null,
//     other: null
//   }
// })

export const defaultState = {
  user: null,
  items: {
    list: [],
    id: null
  },
  users: {
    list: [],
    id: null,
    other: null
  }
}
