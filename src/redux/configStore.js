import { createStore, applyMiddleware, compose } from 'redux'
import { reducer } from './combineReducers'
import { defaultState } from './defaultState'
import createSagaMiddleware from 'redux-saga'
import { initSagas } from './initSagas'
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'

export const getStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  const history = createBrowserHistory()
  const middleWares = [routerMiddleware(history), sagaMiddleware]
  const composables = applyMiddleware(...middleWares)
  const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
    process.env.REACT_APP_ENV !== 'production'
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
          // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        })
      : compose

  const enhancers = composeEnhancers(composables)
  const store = createStore(reducer, defaultState, enhancers)
  initSagas(sagaMiddleware)
  return store
}
