import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import logo from './logo.svg'
import './App.css'
import { usersActions } from './redux/actions/usersActions'
import { Field, reduxForm } from 'redux-form'

function App({ filter, users, ...props }) {
  useEffect(() => {
    props.loadAllUsers()
  }, [users])

  function clickMale() {
    props.setFilter('SHOW_MALE')
  }
  function clickFemale() {
    props.setFilter('SHOW_FEMALE')
  }
  function clickAll() {
    props.setFilter('SHOW_ALL')
  }
  function clickOther() {
    props.setFilter('SHOW_OTHER')
  }
  return (
    <div className='App'>
      <div className='App-header'>
        <div className='row'>
          <button onClick={clickMale} disabled={filter === 'SHOW_MALE'}>
            Show Male {props.male}
          </button>
          <button onClick={clickFemale} disabled={filter === 'SHOW_FEMALE'}>
            Show Female {props.female}
          </button>
          <button onClick={clickOther} disabled={filter === 'SHOW_OTHER'}>
            Show Other {props.other}
          </button>
          <button onClick={clickAll} disabled={filter === 'SHOW_ALL'}>
            Show All {props.all}
          </button>
        </div>
        {users.map(u => (
          <Character key={u.name} {...u} />
        ))}
      </div>
    </div>
  )
}

const setFilter = payload => ({ type: 'SET_VISIBILITY_FILTER', payload })

const mapStateToProps = ({ users, filter }) => ({
  users: filterCharacters(users.list, filter),
  male: users.list.filter(u => u.gender === 'male').length,
  female: users.list.filter(u => u.gender === 'female').length,
  other: users.list.filter(u => u.gender === 'n/a').length,
  all: users.list.length,
  filter
})

const mapDispatchToProps = {
  loadAllUsers: usersActions.loadAllUsersRequest,
  setFilter
}

const AppConnected = reduxForm({
  // a unique name for the form
  form: 'App'
})(App)

export default connect(mapStateToProps, mapDispatchToProps)(AppConnected)

function Character({ name, gender }) {
  return (
    <div>
      <h3>{name}</h3>
      <h5>{gender}</h5>
    </div>
  )
}

function filterCharacters(users, filter) {
  switch (filter) {
    case 'SHOW_ALL':
      return users
    case 'SHOW_MALE':
      return users.filter(u => u.gender === 'male')
    case 'SHOW_FEMALE':
      return users.filter(u => u.gender === 'female')
    case 'SHOW_OTHER':
      return users.filter(u => u.gender === 'n/a')
    default:
      throw new Error('Unknown filter: ' + filter)
  }
}
